# umi template

基于 [umi v3.x](https://umijs.org/zh-CN/docs) 版本创建的前端模版。

# utils 工具集

```
src/utils
├── base64ToBlob.ts
├── camelCase.ts
└── format.ts
└── formData.ts
```

* base64 转 blob。 
* camelCase 驼峰下划线相互转换。
* format 数据转换。
* formData 创建一个 formData 类型并添加参数，并返回 FormData。

# Library 默认库


### [@umijs/hooks](https://hooks.umijs.org/zh-CN)

> Umi Hooks is a React Hooks library dedicated to providing commonly used and high quality Hooks.

### [lodash](https://github.com/lodash/lodash)

> A modern JavaScript utility library delivering modularity, performance, & extras.

### [qs](https://github.com/ljharb/qs)

> A querystring parsing and stringifying library with some added security.

### [js-cookie](https://github.com/js-cookie/js-cookie)

> A simple, lightweight JavaScript API for handling cookies.

### [blueimp-md5](https://github.com/blueimp/JavaScript-MD5)

> JavaScript MD5 implementation.

